<form action="<?= $this->action('save', $model->id) ?>" method="post">
    <div class='row'>
        <div class='col-md-12'>
            <div class="page-header">
                <h2><?= tc('crudix_example', 'Item details') ?></h2>
            </div>
        </div>
    </div>

    <div class='row'>
        <div class='col-md-6'>
            <div class="form-group">
                <label for="name"><?= tc('crudix_example', 'Name') ?></label>
                <input type="text" class="form-control" name="name" value="<?= $model->name ?>" placeholder="<?= tc('crudix_example', 'Item name') ?>">
            </div>

            <div class="form-group">
                <label for="text">Description</label>
                <input type="text" class="form-control" name="description" value="<?= $model->description ?>" placeholder="<?= tc('crudix_example', 'Item description') ?>">
            </div>
        </div>
    </div>

    <div class="ccm-dashboard-form-actions-wrapper">
        <div class="ccm-dashboard-form-actions">
            <a href="<?= URL::to('/dashboard/items') ?>" class="btn btn-danger pull-left"><?= tc('crudix_example', 'Cancel') ?></a>
            <div class="btn-group pull-right">
                <input type="submit" class="btn btn-success" name="add" value="<?= tc('crudix_example', 'Save') ?>" />
                <input type="submit" class="btn btn-default" name="more" value="<?= tc('crudix_example', 'Save and new') ?>" />
            </div>
        </div>
    </div>
</form>