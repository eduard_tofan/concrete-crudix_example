<?php

return [
    'singlepages' => [
        [
            'path' => '/dashboard/items',
            'name' => tc('crudix_example', 'Items'),
            'description' => tc('crudix_example', 'Crudix example crud page.')
        ]
    ]
];