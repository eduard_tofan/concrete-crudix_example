# Advanced Features

- [Columns](#columns)
	- [Displayed Columns](#displayed-columns)
	- [Column Name Mappings](#column-name-mappings)
	- [Pseudo Columns](#pseudo-columns)
	- [The Default Edit Column](#the-default-edit-column)
- [Column Ordering](#column-ordering)
	- [Default Order](#default-order)
	- [Default Ordering Column](#default-ordering-column)
	- [Sortable Columns](#sortable-columns)
- [Pagination](#pagination)
	- [Items Per Page](#items-per-page)
	- [Number Of Pages Displayed](#number-of-pages-displayed)
	- [Items Per Page Filter](#items-per-page-filter)
- [Filters](#filters)
	- [The Default Search Filter](#the-default-search-filter)
	- [The Default Search Filter Fields](#the-default-search-filter-fields)
	- [The Creation Of A Filter](#the-creation-of-a-filter)
	- [Advanced Filters](#advanced-filters)
- [Wrappers](#wrappers)
	- [The Creation Of A Wrapper](#the-creation-of-a-wrapper)
- [Bulk Actions](#bulk-actions)
	- [Default Delete Bulk Action](#default-delete-bulk-action)
	- [Registering A Bulk Action](#registering-a-bulk-action)
	- [Implementing A Bulk Action](#implementing-a-bulk-action)
- [Bulk Actions Formatting](#bulk-actions-formatting)
	- [Bulk Action Alias](#bulk-action-alias)
	- [Bulk Action Default Title](#bulk-action-default-title)
	- [Bulk Action Default Message](#bulk-action-default-message)
- [Model Relations](#model-relations)
	- [Defining The Relationship](#defining-the-relationship)
	- [Passing The Model](#passing-the-model)
	- [Using Relationship Columns](#using-relationship-columns)
	- [Filter By Relationship Columns](#using-relationship-columns)
- [Joins](#joins)
	- [Usage](#usage)
	- [Join Filters](#join-filters)
	
## Columns

#### Displayed Columns

The `Engine` displays all columns selected by default. To display only some of the columns add the following property to your `Crudix\Engine` definition.

> When using joins this property is necessary.

```php
/**
 * The current selected columns.
 *
 * @var array
 */
protected $columns = ['name', 'description'];
```

#### Column Name Mappings

If you want to change the displayed name of a column you can use column mapping. To add or remove mappings add the following method to your `Crudix\Engine` definition.

```php
/**
 * Gets the column mapping.
 *
 * @return array
 */
public function getColumnMapping()
{
    return ['name' => t('Displayed Name')];
}
```

For formatting column values see the [Wrappers](#wrappers) section.

#### Pseudo Columns

"Pseudo" columns are columns with empty content or columns duplicates. Let's say we want the same column twice in our table. We will create a pseudo column and map it to the real column. To add or remove pseudo columns add the following property to your `Crudix\Engine` definition.

```php
 /**
 * The current pseudo-columns.
 *
 * @var array
 */
protected $pseudoColumns = ['edit'];
```

To map a pseudo column to a real column :

```php
/**
 * Gets the pseudo-column mapping.
 *
 * @return array
 */
public function getPseudoColumnMapping()
{
    return ['edit' => 'id'];
}
```

In the example above, we have a pseudo column named `edit` and it will be filled with the data from the `id` column. Next we'll be able to create a wrapper for the pseudo column:

```php
/**
 * Bonus name column wrapper.
 *
 * @param string $value
 * @param \Illuminate\Database\Eloquent\Model $model
 * @return string
 */
public function getEditWrapper($value, $model)
{
    $url = $this->controller->getAddUrl('/' . $model->getKey());

    return '<a href="' . $url . '">' . t('Edit') . '</a>';
}
```

And now we have the `edit` column. In the wrapper you get the row model, basically you can do what you want with pseudo columns.

> You can use column name mappings for pseudo columns.   

> Pseudo columns are not sortable or filterable.


#### The Default Edit Column

The `Engine` has a default `edit` pseudo column. To disable add the following property to your `Crudix\Engine` definition and remove `edit` from the array.

```php
 /**
 * The current pseudo-columns.
 *
 * @var array
 */
protected $pseudoColumns = ['edit'];
```

## Column Ordering

#### Default Order

The `Engine` sorts in ascending order by default. To change the default sort order add the following property to your `Crudix\Engine` definition. Accepted properties [`asc`, `desc`].

```php
/**
 * The default sorting order.
 *
 * @var string
 */
protected $defaultOrder = 'asc';
```

#### Default Ordering Column

The `Engine` sorts after the first selected column by default. To change the default sort column add the following property to your `Crudix\Engine` definition. Following the example from the [quickstart]() guide, let's say we want so sort by `name`.

```php
/**
 * The default sort column.
 * If left unset will sort after first sortable column.
 *
 * @var string
 */
protected $defaultSortColumn = 'name';
```

#### Sortable Columns

 The `Engine` makes all column sortable by default. To change the default sortable columns add the following property to your `Crudix\Engine` definition. Lets's say we want only the `name` column to be sortable.

```php
/**
 * The model columns which are sortable.
 *
 * @var array
 */
protected $sortableColumns = ['name'];
```

## Pagination

#### Items Per Page

The `Engine` has 10 items per page by default. To change the default value add the following property to your `Crudix\Engine` definition.

```php
/**
     * The default number of items per page.
     *
     * @var integer
     */
    protected $itemsPerPage = 15;
```

#### Number Of Pages Displayed

The `Engine` has 10 pages displayed in the pagination bar by default. To change the default value add the following property to your `Crudix\Engine` definition.

```php
/**
 * The default number pages in pagination bar.
 *
 * @var integer
 */
protected $pagesDisplayed = 15;
```

#### Items Per Page Filter

You can even create a filter which is in charge of displaying a certain number of items.
Add your filter to the filters property:

```php
 /**
  * The applied filters.
  *
  * @var array
  */
 protected $filters = ['keyword', 'perPage'];
```

And implement it:


```php
/**
 * Items per page filter.
 *
 * @return \Crudix\Filter
 */
public function getPerPageFilter()
{
    $options = [10, 25, 50];

    return $this->filter('select')
        ->options($options)
        ->closure(function ($query, $value) use ($options) {
            $this->setItemsPerPage($options[$value]);
        });
}
```
    
## Filters

#### The Default Search Filter

The `Engine` has a search filter by default. To disable it add the following property to your `Crudix\Engine` definition and delete the `keyword` field from the array.

```php
 /**
  * The applied filters.
  *
  * @var array
  */
 protected $filters = ['keyword'];
```

#### The Default Search Filter Fields

The `Filter` sorts after the default order column ar the first selected column by default if the order column is not set. To add search fields add the following property to your `Crudix\Engine` definition.
Let's say we want to search after `name` or `description`.

```php
/**
 * The default search filter column.
 * If left unset will filter after default sort column.
 *
 * @var string
 */
protected $defaultFilterColumns = ['name', 'description'];
```

#### The Creation Of A Filter

To create a new filter add a method in your `Crudix\Engine` definition. The method name should be `getSomeNameFilter` where `someName` will be the filter name.

```php
/**
 * Get the someName filter.
 *
 * @return \Crudix\Filter
 */
protected function getSomeNameFilter()
{
    return  $this->filter('text')
        ->fields('description');
}
```
The available chaining methods for a filter are:

- `filter(string)` : `{'text', 'number', 'select', 'selectMultiple', 'checkbox', 'search'}`

- `alias(string)` : the filter name which will be displayed, defaults to readable text. default : `{'someName' => 'Some Name'}`
- `advanced(bool)` : `{'true', 'false'}` displays in advanced section, defaults to `true`

- `fields(array)` : here you will put the column names you want to filter by
- `options(array)` : applicable for select types array of `column_value` => `displayed_value` 
- `miscFields(array)` : additional filter input html attributes

- `label(string)` : the field input label name
- `miscFieldsLabel(array)` : additional label for filter input html attributes   


- `closure(Closure)` : you can pass in a closure action to filter;


#### Advanced Filters

Example usage with a closure:

```php
/**
 * Filters confirmed items.
 *
 * @return \Crudix\Filter
 */
public function getConfirmedFilter()
{
    return $this->filter('select')
        ->options([0 => t('No'), 1 => t('Yes')])
        ->closure(function ($query, $value) {
            $query->where('confirmed', $value);
        }
    );
}
```

- The `query` parameter is the current `\Illuminate\Database\Eloquent\Builder` instance, on which you can apply scopes, and advanced filtering.
- The `value` parameter is the current request value. In out case that can be either `0` or `1`
- The `request`, a third optional in in array with the current request.

> If you want you can even use Eloquent scopes. Implement a confirmed scope on your eloquent model.
 
 
```php
 /**
  * Scope a query to only include confirmed items. 
  * 
  * @return \Illuminate\Database\Eloquent\Builder 
  */ 
 public function scopeConfirmed($query, $flag) 
 { 
     return $query->where('confirmed', $flag); 
 } 
```
 
 Then the filter becomes :
 
```php
 /**
  * Filters confirmed items.
  *
  * @return \Crudix\Filter
  */
 public function getConfirmedFilter()
 {
     return $this->filter('select')
         ->options([0 => t('No'), 1 => t('Yes')])
         ->closure(function ($query, $value) {
             $query->confirmed($value);
         }
     );
 }
```


## Wrappers

#### The Creation Of A Wrapper

Let's say you have a boolean column something like `confirmed` with values `{0, 1}` and you would want to display `'No'` or `'Yes'` instead. Here's how you do it.

To create a new wrapper add a method in your `Crudix\Engine` definition. The method name should be `setConfirmedWrapper` where `confirmed` will be the affected column name.

```php
 /**
 * Item confirmed column wrapper.
 *
 * @param string $value
 * @return string
 */
public function setConfirmedWrapper($value)
{
    return $value ? 'Yes' : 'No';
}
```

If you need the entire row model you can use the other version of the method instead.

```php
 /**
 * Item confirmed column wrapper.
 *
 * @param string $value
 * @param \Illuminate\Database\Eloquent\Model $model
 * @return string
 */
public function setConfirmedWrapper($value, $model)
{
    // do something with the $model object

    return $value ? 'Yes' : 'No';
}
```

## Bulk Actions

#### Default Delete Bulk Action

The `Controller` has a delete bulk action by default. To disable it add the following property to your `Crudix\Api\Controller` definition and delete the `delete` action.

```php
/**
 * The registered bulk actions.
 *
 * @var array
 */
protected $bulkActions = ['delete'];
```

#### Registering A Bulk Action

Let's say we want to register a new bulk action `confirm`, keeping the boolean column defined earlier : `confirmed`. To register it add the following property to your `Crudix\Api\Controller` definition.

```php
/**
 * The registered bulk actions.
 *
 * @var array
 */
protected $bulkActions = ['delete', 'confirm'];
```

#### Implementing A Bulk Action

We defined our bulk action, now let's implement it.
To create a new bulk action add a method in your `Crudix\Api\Controller` definition. The method name should be `bulkConfirmAction` where `confirm` will be the action name.
The `$items` param is an array with the selected items primary keys.

```php
/**
 * Confirms selected entries.
 *
 * @param array $items
 */
public function bulkConfirmAction($items)
{
    $model = $this->engine->getModel()->getModel();

    $model::whereIn($model->getKeyName(), $items)->update(['confirmed' => 1]);
}
```

One way to get the related eloquent method is `$model = $this->engine->getModel()->getModel()`, but it can as easily be done as such :

```php
/**
 * Confirms selected entries.
 *
 * @param array $items
 */
public function bulkConfirmAction($items)
{
    Item::whereIn('id', $items)->update(['confirmed' => 1]);
}
```

Where `Item` is the name of your related eloquent model and `id` is the models primary key.

## Bulk Actions Formatting

#### Bulk Action Alias

The default display name of the action (alias) will be a readable name : `confirm` => `Confirm`, `confirmAction` => `Confirm Action`.

To set an alias you need to add the following method to your `Crudix\Api\Controller` definition.

```php
/**
 * Gets the bulk actions mapping.
 * Available keys {alias|title|message}
 *
 * @return array
 */
public function getBulkActionMapping()
{
    return ['confirm' => ['alias' => 'Confirm Action']];
}
```

The same can be done to customize the confirmation pop-up title and message.

#### Bulk Action Default Title

The default action pop-up title can be change by overriding the following method in your `Crudix\Api\Controller` definition.

```php
/**
 * Get the default bulk action dialog title.
 *
 * @return string
 */
public function getBulkActionTitle()
{
    return t('Are you sure ?');
}
```

#### Bulk Action Default Message

The default action pop-up title can be change by overriding the following method in your `Crudix\Api\Controller` definition.

```php
 /**
 * Get the default bulk action dialog message.
 *
 * @return string
 */
public function getBulkActionMessage()
{
    return t('You are going to {action} {count} items.');
}
```

The `{action}` parameter will be replaced with the action name if exists, and the `{count}` with the selected count if exists.
None of the parameters are required.

## Model Relations

You can use eloquent relations with the CRUD `Engine`.

Let's say we have a many-to-many relationship between our `Item` model an another `Role` model.

#### Defining The Relationship

First we need to define our `roles` relationship. In your `Item` eloquent Model add :

```php
/**
 * Get the roles relationship.
 *
 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
 */
public function roles()
{
    return $this->belongsToMany('CrudixExample\Models\Role', 'five_crudix_items_roles');
}
```

#### Passing The Model

Now that we have the relationship we need to make sure that we pass it to our `Engine` instance. Modify the `getModel()` method and load the relationship : 

```php
/**
 * Set the crud table Eloquent select.
 *
 * @return \Illuminate\Database\Eloquent\Builder;
 */
public function getModel()
{
    return CrudixExample\Models\Item::with('roles')->select('*');
}
```

> Make sure to be include the relationship when returning the model. If you don't you won't be able to use relationship columns.

#### Using Relationship Columns

Once you have have the relationship an the model with the relationship loaded you can use it's columns.

> The notation used is the `dot` notation. If we want the `name` column from our `roles` relationship, the column name will be `roles.name`.

```php
/**
 * The current selected columns.
 *
 * @var array
 */
protected $relationColumns = ['roles.name'];
```

> Relationship columns, as pseudo-columns are not sortable.

#### Filter By Relationship Columns

To filter a relationship column, register and create a filter method.

```php
/**
 * The applied filters.
 *
 * @var array
 */
protected $filters = ['keyword', 'role'];
```

And the method :

```php
/**
 * Filters by role.
 *
 * @return \Crudix\Filter
 */
public function getRoleFilter()
{
    $names = Role::get()->pluck('name')->toArray();

    return $this->filter('select')
        ->options(array_combine($names, $names))
        ->closure(function ($query, $value) {
            $query->whereHas('roles', function ($query) use ($value) {
                $query->where($query->getModel()->getTable() . '.name', $value);
            });
        });
}
```

So first we get all available roles names:

```php
$names = Role::get()->pluck('name')->toArray();
```

Then we create a select filter with the names as options, and we filter the relationship using a `closure()`.   

We use the classic relationship filtering from eloquent: 

```php
$query->whereHas('roles', function ($query) use ($value) {
    $query->where($query->getModel()->getTable() . '.name', $value);
});
```

> Keep in mind that if you have duplicate column names you must pass the column name as such : `table_name.column_name`.

> Also when in `whereHas` scope we must specify the table name when selecting relationship column. `$query->getModel()->getTable()` translates to `five_crudix_roles` because that is the table associated with the `Role` model.


## Joins

If you prefer using joins to relationship or you can't do something with relations, you can always use joins.

Lets re-enact a many-to-many relationship using joins.

#### Usage
 
It's good practice to define a method in your eloquent model, in our case `Item` which returns the joined model.

```php
/**
 * Gets the start select
 *
 * @return \Illuminate\Database\Eloquent\Builder
 */
public function getSelect()
{
    return $this->join('five_crudix_items_roles', 'five_crudix_items.id', '=', 'five_crudix_items_roles.item_id')
        ->join('five_crudix_roles', 'five_crudix_items_roles.role_id', '=', 'five_crudix_items_roles.role_id')
        ->select(['five_crudix_items.*', 'five_crudix_roles.name as role_name'])->with('roles');
}
```

> Keep in mind that when using joins you must use column aliases if you have 2 columns with the same name.   

> When using joins you will get all rows combinations, when using relations you will get a row with an array of the joined table column values.

Now lets pass in our eloquent model :

```php
/**
 * Set the crud table Eloquent select.
 *
 * @return \Illuminate\Database\Eloquent\Builder;
 */
public function getModel()
{
    return (new CrudixExample\Models\Item)->getSelect();
}
```

Now we should have all columns of the join. Lets select the `five_crudix_roles.name` column. As you can see we selected it with an alias because we already have a `name` column on our base table.

```php
/**
 * The current selected columns.
 *
 * @var array
 */
protected $columns = ['id', 'name', 'description', 'confirmed', 'role_name'];
```
 
> Notice that we did NOT use the `table_name.column_name` for the columns name.

> If you want a join column add the column name in the array.

> If you have duplicates use aliases.

> Join columns are not sortable.

> You CAN filter join columns.

#### Join Filters

```php
/**
 * Filters by role.
 *
 * @return \Crudix\Filter
 */
public function getRoleFilter()
{
    $names = ['Administrator', 'User', 'Other'];

    return $this->filter('select')
        ->options(array_combine($names, $names))
        ->closure(function ($query, $value) {
            $query->where('five_crudix_roles.name', $value);
        });
}
```

> Notice that in the filter we used the notation `table_name.column_name`. If we don't do this the query will throw an `Ambiguous column name` error.
