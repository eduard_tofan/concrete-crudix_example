<?php

namespace Concrete\Package\CrudixExample\Controller\SinglePage\Dashboard;

use CrudixExample\Models\Item;
use CrudixExample\Crud\Controllers\Item as ItemCrud;
use Concrete\Core\Page\Controller\DashboardPageController;

class Items extends DashboardPageController
{
    /**
     * Override for view function
     */
    public function view()
    {
        $response = ItemCrud::instance()->getView();

        $this->set('response', $response);
    }

    /**
     * Renders the edit/add page.
     *
     * @param int|string|null $id
     */
    public function edit($id = null)
    {
        $model = Item::find($id);

        if ($model) {
            $this->set('model', $model);
        }

        $this->render('/dashboard/items/edit');
    }

    /**
     * Handles updating and adding.
     *
     * @param int|null $id
     */
    public function save($id = null)
    {
        $model = Item::firstOrNew(['id' => $id]);

        $model->fill([
            'name' => $this->post('name'),
            'description' => $this->post('description')
        ])->save();

        $this->oneMore($model->id);
    }

    /**
     * Should or should not create one more entry.
     *
     * @param int|string $primary
     */
    private function oneMore($primary)
    {
        if ($this->post('more')) {
            $this->redirect('/dashboard/items/edit');
        }

        $this->redirect("/dashboard/items/edit/{$primary}");
    }
}