<?php

namespace Concrete\Package\CrudixExample;

use FiveToolkit\Package\Start;
use FiveToolkit\Package\Install;
use Concrete\Core\Package\Package;

define('CRUDIX_EXAMPLE_PACKAGE', 'crudix_example');

class Controller extends Package
{
    /**
     * Package handle.
     *
     * @var string
     */
    protected $pkgHandle = CRUDIX_EXAMPLE_PACKAGE;

    /**
     * The current package version.
     *
     * @var string
     */
    protected $pkgVersion = '1.0.0';

    /**
     * Autoload the src folder under
     * the Crudix namespace.
     *
     * @var array
     */
    protected $pkgAutoloaderRegistries = ['src' => '\CrudixExample'];

    /**
     * Get the package name.
     *
     * @return string
     */
    public function getPackageName()
    {
        return t('Crudix Example');
    }

    /**
     * Get the package description.
     *
     * @return string
     */
    public function getPackageDescription()
    {
        return t('Laravel model based CRUD tool example.');
    }

    /**
     * Override for the package start
     * function.
     */
    public function on_start()
    {
        $this->autoloadComposer();

        Start::start($this);
    }

    /**
     * Package install override.
     *
     * @return Package
     */
    public function install()
    {
        // for some reason install does not call
        // on_start and does not load the autoloader
        $this->autoloadComposer();

        $pkg =  parent::install();
        Install::install($pkg);

        return $pkg;
    }

    /**
     * Autoload composer files.
     */
    protected function autoloadComposer()
    {
        $file = "{$this->getPackagePath()}/vendor/autoload.php";

        if (file_exists($file)) {
            require_once $file;
        }
    }
}
