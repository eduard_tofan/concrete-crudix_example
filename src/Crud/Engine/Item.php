<?php

namespace CrudixExample\Crud\Engine;

use Crudix\Engine;
use CrudixExample\Models\Permission;
use CrudixExample\Models\Role;

class Item extends Engine
{
    /**
     * The current selected columns.
     *
     * @var array
     */
    protected $columns = ['id', 'name', 'confirmed'];

    /**
     * The current relation selected columns.
     *
     * @var array
     */
    protected $relationColumns = ['roles.name', 'roles.permissions.name'];

    /**
     * The applied filters.
     *
     * @var array
     */
    protected $filters = ['keyword', 'confirmed', 'role', 'permission', 'perPage'];

    /**
     * Set the crud table Eloquent select.
     *
     * @return \Illuminate\Database\Eloquent\Builder;
     */
    public function getModel()
    {
        return \CrudixExample\Models\Item::with('roles.permissions')->select('*');
    }

    /**
     * Filters confirmed items.
     *
     * @return \Crudix\Filter
     */
    public function getConfirmedFilter()
    {
        $options = [t('No'), t('Yes')];

        return $this->filter('select')
            ->fields(['confirmed'])
            ->options($options);
    }

    /**
     * Filters by role.
     *
     * @return \Crudix\Filter
     */
    public function getRoleFilter()
    {
        $names = Role::get()->pluck('name', 'id')->toArray();

        return $this->filter('select')
            ->options($names)
            ->closure(function ($query, $value) {
                $query->whereHas('roles', function ($query) use ($value) {
                    $query->where("{$query->getModel()->getTable()}.id", $value);
                });
            });
    }

    /**
     * Filters by permission.
     *
     * @return \Crudix\Filter
     */
    public function getPermissionFilter()
    {
        $names = Permission::get()->pluck('name', 'id')->toArray();

        return $this->filter('select')
            ->options($names)
            ->closure(function ($query, $value) {
                $query->whereHas('roles.permissions', function($query) use ($value) {
                    $query->Where("{$query->getModel()->getTable()}.id", $value);
                });
            });
    }

    /**
     * Items per page filter.
     *
     * @return \Crudix\Filter
     */
    public function getPerPageFilter()
    {
        $options = [10, 25, 50];

        return $this->filter('select')
            ->options($options)
            ->closure(function ($query, $value) use ($options) {
                $this->setItemsPerPage($options[$value]);
            });
    }
}
