<?php

namespace CrudixExample\Crud\Controllers;

use Crudix\Api\Controller;

class Item extends Controller
{
    /**
     * The base path on which all routes and urls will be made.
     *
     * @var string
     */
    protected $baseRoute = '/dashboard/items';

    /**
     * The registered bulk actions.
     *
     * @var array
     */
    protected $bulkActions = ['delete', 'confirm'];

    /**
     * Set up the crud engine.
     *
     * @return \Crudix\Engine $engine
     */
    protected function getEngine()
    {
        return new \CrudixExample\Crud\Engine\Item;
    }

    /**
     * Confirms selected entries.
     *
     * @param array $items
     */
    public function bulkConfirmAction($items)
    {
        $model = $this->engine->getModel()->getModel();

        $model::whereIn($model->getKeyName(), $items)->update(['confirmed' => 1]);
    }
}
