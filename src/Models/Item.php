<?php

namespace CrudixExample\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'five_crudix_items';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Scope a query to only include confirmed items.
     *
     * @param $query
     * @param $flag
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeConfirmed($query, $flag)
    {
        return $query->where('confirmed', $flag);
    }

    /**
     * Get the roles relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('CrudixExample\Models\Role', 'five_crudix_items_roles');
    }

    /**
     * Gets the start select for the join example.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getSelect()
    {
        return $this->join('five_crudix_items_roles', 'five_crudix_items.id', '=', 'five_crudix_items_roles.item_id')
            ->join('five_crudix_roles', 'five_crudix_items_roles.role_id', '=', 'five_crudix_items_roles.role_id')
            ->select(['five_crudix_items.*', 'five_crudix_roles.name as role_name']);
    }
}
